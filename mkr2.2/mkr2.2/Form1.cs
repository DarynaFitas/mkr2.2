﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mkr2._2
{
    public partial class Form1 : Form
    {

        List<Student> StudentList;
        Student[] stud_srr = new Student[5];
        BindingSource BindSource;

        public Form1()
        {
            InitializeComponent();
            StudentInfo.RowCount = 5;
            StudentInfo.ColumnCount = 8;
    }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void RenderTable()
        {
            for(int i = 0;i< stud_srr.Length;i++)
            {
                StudentInfo.Rows[i].Cells[0].Value = stud_srr[i].Surname;
                StudentInfo.Rows[i].Cells[1].Value = stud_srr[i].Faculty;
                StudentInfo.Rows[i].Cells[2].Value = stud_srr[i].Curs;
                StudentInfo.Rows[i].Cells[3].Value = stud_srr[i].Gender;
                StudentInfo.Rows[i].Cells[4].Value = stud_srr[i].Stependia;
                StudentInfo.Rows[i].Cells[5].Value = stud_srr[i].Ball1;
                StudentInfo.Rows[i].Cells[6].Value = stud_srr[i].Ball2;
                StudentInfo.Rows[i].Cells[7].Value = stud_srr[i].Ball3;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < stud_srr.Length; i++)
            {
                if (stud_srr[i].Surname == textBox1.Text)
                {
                    label1.Text = $"Прізвище  {stud_srr[i].Surname} Стипендія: {stud_srr[i].Stependia} Бали: {stud_srr[i].Ball1},{stud_srr[i].Ball2},{stud_srr[i].Ball3}";
                    break;
                }
                else
                {
                    label1.Text = "Не знайдено";
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            StudentList = new List<Student>();
            stud_srr[0] = new Student("Коваленко", "Математичний", "1", "Жіноча", "2500", "96", "56", "74");
            stud_srr[1] = new Student("Юсенков", "Юридичний", "2", "Чоловіча", "2000", "95", "65", "78");
            stud_srr[2] = new Student("Мац", "Фізичний","3", "Чоловіча", "2000", "21", "10", "2" );
            stud_srr[3] = new Student("Енубілев", "Математичний","4", "Жіноча", "2500", "67", "76", "98");
            stud_srr[4] = new Student("Кривка", "Фізичний","1", "Жіноча", "2500", "100", "96", "95");
            StudentList.Add(new Student("Коваленко", "Математичний", "1", "Жіноча", "2500", "96", "56", "74"));
            StudentList.Add(new Student("Юсенков", "Юридичний", "2", "Чоловіча", "2000", "95", "65", "78"));
            StudentList.Add(new Student("Мац", "Фізичний", "3", "Чоловіча", "2000", "21", "10", "2"));
            StudentList.Add(new Student("Енубілев", "Математичний", "4", "Жіноча", "2500", "67", "76", "98"));
            StudentList.Add(new Student("Кривка", "Фізичний", "1", "Жіноча", "2500", "100", "96", "95"));
            BindSource = new BindingSource();
            BindSource.DataSource = StudentList;

            StudentInfo.DataSource = BindSource;

            StudentInfo.Columns["surname"].HeaderText = "Прізвище";
            StudentInfo.Columns["faculty"].HeaderText = "Факультет";
            StudentInfo.Columns["curs"].HeaderText = "Курс";
            StudentInfo.Columns["gender"].HeaderText = "Стать";
            StudentInfo.Columns["stependia"].HeaderText = "Стипендія";
            StudentInfo.Columns["ball1"].HeaderText = "Оцінка 1";
            StudentInfo.Columns["ball2"].HeaderText = "Оцінка 2";
            StudentInfo.Columns["ball3"].HeaderText = "Оцінка 3";



        }
    }
}
