﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mkr2._2
{
    internal class Student
    {
        public string Surname { get; set; }
        public string Faculty { get; set; }
        public string Curs { get; set; }
        public string Gender { get; set; }
        public string Stependia { get; set; }
        public string Ball1 { get; set; }
        public string Ball2 { get; set; }
        public string Ball3 { get; set;}
        public Student(string surname, string faculty, string curs, string gender, string stependia, string ball1, string ball2, string ball3)
        {
            Surname = surname;
            Faculty = faculty;
            Curs = curs;
            Gender = gender;
            Stependia = stependia;
            Ball1 = ball1;
            Ball2 = ball2;
            Ball3 = ball3;
        }
    }
}
